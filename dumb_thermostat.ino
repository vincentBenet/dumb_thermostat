#include <IRremote.h>
#include <LiquidCrystal.h>
#include "DHT.h"

float humidite;

int ordre_humidite=75; // % 
float humidite_evolution=0.3; // pourcent par temps
int timer_humidity_stop=2; // minutes 
int force_on = 1;
int state_relay = 0;
long unsigned int time_on = 0;
long unsigned int time_on_saved = 0;

long unsigned int saved_time = 0;
float saved_humidity = 0;
float evo = 0;
const int pin_relai=8;
const int pin_capteurTH=7;
const int pin_capteurIR=6;
const int pin_lcd1=12;
const int pin_lcd2=11;
const int pin_lcd3=5;
const int pin_lcd4=4;
const int pin_lcd5=3;
const int pin_lcd6=2;

const int frequence_echantillonage=100;

int curseur=1;
int IRcommande;

IRrecv irrecv(pin_capteurIR);
decode_results results;
DHT dht(pin_capteurTH,DHT22);
LiquidCrystal lcd(pin_lcd1,pin_lcd2,pin_lcd3,pin_lcd4,pin_lcd5,pin_lcd6);

void setup() {
  Serial.begin(9600);
  irrecv.enableIRIn();
  dht.begin();
  lcd.begin(16, 2);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pin_relai, OUTPUT);
}

void affiche(){
  Serial.print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
  Serial.print("humidite: ");Serial.println(humidite);
  Serial.print("ordre_humidite: ");Serial.println(ordre_humidite);
  Serial.print("humidite_evolution: ");Serial.println(humidite_evolution);
  Serial.print("timer_humidity_stop: ");Serial.println(timer_humidity_stop);
  Serial.print("force_on: ");Serial.println(force_on);
  Serial.print("state_relay: ");Serial.println(state_relay);
  Serial.print("evo: ");Serial.println(evo);
  Serial.print("curseur: ");Serial.println(curseur);
  Serial.print("IRcommande: ");Serial.println(IRcommande); 
  Serial.print("millis: ");Serial.println(millis()); 
  Serial.print("saved_time: ");Serial.println(saved_time);   
  Serial.print("saved_humidity: ");Serial.println(saved_humidity);
  Serial.print("time_on: ");Serial.println(time_on);
  Serial.print("time_on_saved: ");Serial.println(time_on_saved);
}

void affiche_LCD(){
  // Humidity sensor
  lcd.setCursor(0,0);lcd.print("H ");lcd.print(humidite); lcd.setCursor(6,0);lcd.print("| ");lcd.print(ordre_humidite);lcd.setCursor(10,0);lcd.print("% T ");lcd.print(timer_humidity_stop);
  
  lcd.setCursor(0,1);lcd.print("E ");lcd.print(evo);      lcd.setCursor(6,1);lcd.print("| ");lcd.print(humidite_evolution);lcd.setCursor(11,1);lcd.print(" ");lcd.print(float(time_on)/3600);

  
  if ((curseur==0 or force_on == 1) and force_on != 2){
    lcd.setCursor(7,0);lcd.print("~");
  }
  if ((curseur==1 or force_on == 1) and force_on != 2){
    lcd.setCursor(13,0);lcd.print("~");
  } 
  if ((curseur==2 or force_on == 1) and force_on != 2){
    lcd.setCursor(7,1);lcd.print("~");
  }  
}
void IR(){
  if (irrecv.decode(&results)) {
    IRcommande=results.value;
    if (IRcommande==20655){//droite
      curseur = (curseur + 1) % 3;
    }
    if (IRcommande==4335){//gauche
      curseur = (curseur - 1) % 3;
    }
    if (IRcommande==-28561){  // Milieu
      force_on = (force_on + 1) % 3;
    }
  
  
    if (IRcommande==-24481){ // plus
      if (curseur==0){
        ordre_humidite=min(99,ordre_humidite+1);
      }
      if (curseur==1){
        timer_humidity_stop=min(99,timer_humidity_stop+1);
      }
      if (curseur==2){
        humidite_evolution=min(99,humidite_evolution + 0.1);
      }
    }
    if (IRcommande==-20401){ // moins
      if (curseur==0){
        ordre_humidite=max(0,ordre_humidite-1);
      }
      if (curseur==1){
        timer_humidity_stop=max(0,timer_humidity_stop-1);
      }
      if (curseur==2){
        humidite_evolution=max(0,humidite_evolution - 0.1);
      }
    }
    Serial.println(IRcommande);
    irrecv.resume();
  };
}
void relai(){
  if (
    (
      humidite > ordre_humidite and
      force_on == 0
    ) or
    (
      evo > humidite_evolution and
      force_on == 0
    ) or 
    (
      force_on == 1
    )
  ){
      digitalWrite(pin_relai,HIGH);
      digitalWrite(LED_BUILTIN,HIGH);
      if (state_relay == 0){
        time_on_saved = int(millis()/1000);
      }
      time_on = int(millis()/1000 - time_on_saved);
      state_relay = 1;
      
  }
  if (
    (
      evo >= 0 and 
      humidite < ordre_humidite and 
      force_on == 0
    ) or 
    (
      force_on == 2
    )
  ){
    digitalWrite(pin_relai,LOW);
    digitalWrite(LED_BUILTIN,LOW);
    state_relay = 0;
    time_on = 0;
  }
}
void mesure(){
  humidite=dht.readHumidity();
  if ((millis()/1000 - saved_time)>timer_humidity_stop*60){
    evo = float(humidite - saved_humidity);
    saved_humidity = humidite;
    saved_time = int(millis()/1000);
  }
}
void loop() {
  mesure();
  IR();
  affiche();
  affiche_LCD();
  relai();
  delay(frequence_echantillonage);
}
